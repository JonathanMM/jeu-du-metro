/*
Copyright 2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Promise = require('promise');

app.use(express.static('lib/editor/public'));

module.exports = function(db, port) {
	http.listen(port, function(){
		console.log('*E* [EXPRESS] Serveur démarré sur le port '+port);
	});

	Station = db.Station;
	Ligne = db.Ligne;
	Trajet = db.Trajet;
	Quete = db.Quete;
	Etat = db.Etat;
	Condition = db.Condition;
	Dialogue = db.Dialogue;

	var listeClasses = {
		Ligne: {attr: {nom: 'text'}, assoInterne: {}, assoExterne: {StationSurLigne: 'Station'}, classe: Ligne},
		Station: {attr: {nom: 'text'}, assoInterne: {}, assoExterne: {StationSurLigne: 'Ligne'}, classe: Station},
		Trajet: {attr: {duree: 'number'}, assoInterne: {departId: 'Station', arriveeId: 'Station', ligneId: 'Ligne'}, assoExterne: {}, classe: Trajet},
		Quete: {attr: {nom: 'text'}, assoInterne: {conditionId: 'Condition'}, assoExterne: {}, classe: Quete},
		Etat: {attr: {final: 'boolean', resume: 'textarea', gainXp: 'number'}, assoInterne: {queteId: 'Quete'}, assoExterne: {ConditionDEtat: 'Condition'}, classe: Etat},
		Dialogue: {attr: {nomPersonnage: 'text', texte: 'textarea'}, assoInterne: {stationId: 'Station', etatId: 'Etat'}, assoExterne: {}, classe: Dialogue},
		Condition: {attr: {niveau: 'number'}, assoInterne: {etatId: 'Etat', stationId: 'Station'}, assoExterne: {ConditionDEtat: 'Etat'}, classe: Condition}
	};

	//Lancement du serveur socket.io
	io.on('connection', function(socket){
		socket.on('listeClasses', function() {
			var obj = {};
			for(var key in listeClasses)
			{
				obj[key] = {attr: listeClasses[key].attr, assoInterne: listeClasses[key].assoInterne, assoExterne: listeClasses[key].assoExterne};
			}
			socket.emit('listeClasses', obj);
		});

		socket.on('contenu', function(c) {
			var classe = listeClasses[c];
			classe.classe.all().then(function(items) {
				var result = [];
				Promise.all(
					items.map(function(item) {
						return new Promise(function (resolve, reject) {
							var obj = {id: item.id};
							Object.keys(classe.attr).forEach(
								function(attr)
								{
									obj[attr] = item[attr];
								}
							);
							Object.keys(classe.assoInterne).forEach(
								function(attr)
								{
									obj[attr] = item[attr];
								}
							);
							return resolve(obj);
						}).then(function(obj) {
							if(Object.keys(classe.assoExterne).length == 0)
								result.push(obj);
							else
							{
								return Promise.all(
									Object.keys(classe.assoExterne).map(function(key) {
										var classeDemande = classe.assoExterne[key];
										return item['get'+classeDemande+'s']().then(function(elements) {
											obj[key] = elements.map(function(elt) { 
												return elt[key][classeDemande.toLowerCase()+'Id'];
											});
											return obj;
										});
									})
								).then(function(obj) {
									result.push(obj[0]);
								});
							}
						});
					})
				).then(function() {
					socket.emit('contenu', {classe: c, items: result});		
				});
			});
		});

		//On traite les infos à mettre en bdd
		socket.on('putInfo', putInfo);
		socket.on('putInfos', function(listeInfos) {
			Promise.all(
				listeInfos.map(function(item) { putInfo(item, false); })
			).then(function() {
				socket.emit('putInfos', true);
			});
		});

		function putInfo(infos, sendInfo) {
			if(!!sendInfo)
				sendInfo = true;
			var id = infos.id;
			var classe = infos.classe;
			var values = infos.values;
			var valuesExt = infos.valuesExt;
			if(!!listeClasses[classe])
			{
				var dbc = listeClasses[classe].classe;
				if(id == 0 || !id) //Nouvelle donnée
				{
					return dbc.create(values).then(function(item) {
						if(!(Object.keys(valuesExt).length === 0))
						{
							for(var key in valuesExt)
							{
								var val = valuesExt[key];
								var classeDemande = listeClasses[classe].assoExterne[key];
								item['set'+classeDemande+'s'](val).then(function() {
									if(sendInfo)
										socket.emit('updateNeeded', classe);
								});
							}
						} else
							if(sendInfo)
								socket.emit('updateNeeded', classe);
					});
				} else {
					if(values == 'delete')
					{
						return dbc.destroy({where: {id: id}}).then(function(item) {
							if(sendInfo)
								socket.emit('updateNeeded', classe);
						});
					} else {
						values.id = id;
						return dbc.upsert(values, {where: {id: id}}).then(function(items) {
							if(valuesExt != undefined && !(Object.keys(valuesExt).length === 0))
							{
								dbc.findById(id).then(function(item) {
									for(var key in valuesExt)
									{
										var val = valuesExt[key];
										var classeDemande = listeClasses[classe].assoExterne[key];
										item['set'+classeDemande+'s'](val).then(function() {
											if(sendInfo)
												socket.emit('updateNeeded', classe);
										});
									}
								});
							} else {
								if(sendInfo)
									socket.emit('updateNeeded', classe);
							}
						});
					}
				}
			}
		}
	});
};
