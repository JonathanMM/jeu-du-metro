/*
Copyright 2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/
demanderClasses();
//On va récupérer ce qu'il faut pour les données déjà stockés
var classesNecessaire = ["Ligne", "Station", "Etat", "Condition"];
var listeEtats;
var maxIdCondition = -1;
for(var i in classesNecessaire)
	socket.emit('contenu', classesNecessaire[i]);
//On redéfini la fonction d'affichage
afficherLien = false;

function creerLigneTableau(tag, noms)
{
	var tr = document.createElement('tr');
	for(var i in noms)
	{
		var nom = noms[i];
		var th = document.createElement(tag);
		if(Array.isArray(nom))
		{
			for(var i in nom)
			{
				th.appendChild(nom[i]);
			}
		} else {
			if(typeof nom != "object")
				th.innerHTML = nom;
			else
				th.appendChild(nom);
		}
		tr.appendChild(th);
	}
	return tr;
}

function getMaxId(liste)
{
	var max = -1;
	Object.keys(liste).forEach(function(item) { if(parseInt(item) > max) max = parseInt(item) });
	return max;
}

function updateListeEtat()
{
	var listeNode = document.querySelectorAll('.select-Etat');
	for(var i in listeNode)
	{
		if(!Object.is(parseInt(i),NaN))
		{
			var elt = listeNode[i];
			var valueSelectedElement = elt.options[elt.selectedIndex].value;
			elt.innerHTML = '';
			ajouterOptionsDansSelect(elt, listeEtats, valueSelectedElement, true);
		}
	}
}

function faireCommande()
{
	var button = document.createElement('button');
	button.innerHTML = 'Ajouter en dessous';
	button.addEventListener('click', function() {
		var table = document.getElementById('listeEtats');
		var trCurrent = this.parentNode.parentNode;
		var nextIndex = Math.max(getMaxId(contenuStocke.Etat), getMaxId(listeEtats)) + 1;
		var nextIndexCondition = Math.max(getMaxId(contenuStocke.Condition), maxIdCondition) + 1;
		maxIdCondition = nextIndexCondition;
		var nouvelEtat = { final: true, resume: null, gainXp: null, queteId: -1, ConditionDEtat: [] };
		listeEtats[nextIndex] = nouvelEtat;
		updateListeEtat();
		var tds = trCurrent.getElementsByTagName('td');
		var idCourant = parseInt(tds[0].innerHTML);
		if(Object.is(idCourant, NaN))
			idCourant = 0;
		var choixStation = faireChamp('Station-'+idCourant, 'lien', null, "Station", false);
		var choixEtat = faireChamp('Destination-'+idCourant, 'lien', nextIndex, "Etat", false, listeEtats);
		var colonnes = getColonnes();
		var typeColonnes = getTypeColonnes(colonnes);

		//On met à jour la ligne courante en conséquence
		var elt = tds[tds.length - 2].querySelector('.select-Etat');
		tds[tds.length - 2].innerHTML = '';
		tds[tds.length - 2].appendChild(choixEtat);

		var tdsSuivant = trCurrent.nextSibling.getElementsByTagName('td');
		var choixStation = faireChamp('Station-'+nextIndex, 'lien', null, "Station", false);
		var choixEtatCourant = faireChamp('Destination-'+nextIndex, 'lien', parseInt(tdsSuivant[0].innerHTML), "Etat", false, listeEtats);
		var cellules = [];
		//TODO : Code suivant à modifier pour coller à creerCellulesAttributs (qui sera aussi à modifier en conséquence, avec, par exemple, des noms d'arguments plus explicite…)
		for(var i in typeColonnes)
		{
			i = parseInt(i);
			if(i <= 2)
			{
				var colonne = colonnes[i+1];
				var typeColonne = typeColonnes[i];
				if(typeColonne.type == undefined)
				{
					cellules.push(faireChamp(colonne+'-'+nextIndex, typeColonne, null, null, false));
				}
			}
		}
		var td = creerLigneTableau('td', [nextIndex].concat(cellules).concat([
			choixStation,
			choixEtatCourant,
			faireCommande()
		]));
		table.insertBefore(td, trCurrent.nextSibling);
	});
	return button;
}

function creerCellulesAttributs(typeColonnes, colonnes, condition, etatPrecedent, etatId)
{
	var cellules = [];
	var conditionId = condition.id;
	var etatPrecedentId = etatPrecedent.id;
	for(var i in typeColonnes)
	{
		i = parseInt(i);
		var colonne = colonnes[i+1];
		var typeColonne = typeColonnes[i];
		if(typeColonne.type == undefined)
		{
			if(etatPrecedentId == 0)
				cellules.push("");
			else
				cellules.push(faireChamp(colonne+'-'+etatPrecedentId, typeColonne, etatPrecedent[colonne], null, false));
		}
		else
		{
			var type = typeColonne.type;
			if(type == 'buttonsystem')
			{
				//Boutons de commande
				cellules.push([
					faireCommande(),
					faireChamp('€idCondition-'+etatPrecedentId, 'cache', conditionId)
				]);
			}
			else
			{
				cellules.push(faireChamp(colonne+'-'+etatPrecedentId, type, (typeColonne.lien == 'Etat' ? etatId : condition[colonne.toLowerCase()+'Id']), typeColonne.lien, false, (typeColonne.lien == 'Etat' ? listeEtats : null)));
			}
		}
	}

	cellules.push(etatId);
	return cellules;
}

function getTypeColonnes(colonnes)
{
	var typeColonnes = [];
	for(var i = 1; i < colonnes.length - 3; i++)
		typeColonnes.push(listeClasses.Etat.attr[colonnes[i]]);
	typeColonnes.push({type: 'lien', lien: 'Station'});
	typeColonnes.push({type: 'lien', lien: 'Etat'});
	typeColonnes.push({type: 'buttonsystem'});

	return typeColonnes;
}

function getColonnes()
{
	var colonnes = ['Nom'];
	colonnes = colonnes.concat(Object.keys(listeClasses.Etat.attr));
	colonnes = colonnes.concat(['Station', 'Destination', '']);

	return colonnes;
}

affichageFormulaireCustom = function(classe, id)
{
	if(id == 0)
	{
		alert('Pas encore implémenté !');
		return false;
	}
	var formulaire = document.getElementById('formulaire');
	//On va construire le tableau d'affichagew
	var div = document.createElement('div');
	var table = document.createElement('table');
	table.id='listeEtats';
	var colonnes = getColonnes();
	table.appendChild(creerLigneTableau('th', colonnes));
	var typeColonnes = getTypeColonnes(colonnes);

	//Puis, on rempli ce tableau
	var values = contenuStocke[classe][id];
	//On commence par la condition de base
	var conditionAAjouter = [];
	listeEtats = [];
	var cellules;
	if(!!values)
	{
		var conditionInitId = values.conditionId;
		conditionAAjouter.push(conditionInitId);
		var etatDejaVu = [];
		cellules = ['&lt;début&gt;'];
		var etatPrecedentId = 0;
		var etatPrecedent = {id: 0};
		while(conditionAAjouter.length != 0)
		{
			var conditionId = conditionAAjouter.shift();
			var condition = contenuStocke.Condition[conditionId];
			condition.id = conditionId;
			if(!!condition && etatDejaVu.indexOf(condition.etatId) == -1)
			{
				var etatId = condition.etatId;
				var etat = contenuStocke.Etat[etatId];
				etat.id = etatId;
				listeEtats[etatId] = etat;

				//Puis on ajoute les états un à un
				cellules = cellules.concat(creerCellulesAttributs(typeColonnes, colonnes, condition, etatPrecedent, etatId));
				etatPrecedentId = etatId;
				etatPrecedent = etat;
				etatDejaVu.push(etatId);
				var final = etat.final;
				if(final)
				{
					for(var i in typeColonnes)
					{
						i = parseInt(i);
						if(typeColonnes[i].type == undefined)
						{
							if(etatPrecedent == null)
								cellules.push("");
							else
								cellules.push(faireChamp(colonnes[i + 1]+'-'+etatPrecedentId, typeColonnes[i], etatPrecedent[colonnes[i+1]], null, false));
						}
					}
					cellules.push("");
					cellules.push('&lt;fin&gt;');
					cellules.push("");
				} else {
					//On regarde les conditions d'états
					for(var i in etat.ConditionDEtat)
					{
						var cde = etat.ConditionDEtat[i];
						conditionAAjouter.push(cde);
					}
				}
			}
		}
	} else {
		cellules = ['&lt;début&gt;', 'Oui', '', '&lt;fin&gt;'];
	}

	//Et maintenant, y a plus qu'à faire les lignes
	var celluleParLigne = colonnes.length;
	var i = 0;
	while(i < cellules.length)
	{
		var ligne = [];
		for(var j = 0; j < celluleParLigne; j++)
		{
			ligne.push(cellules[i]);
			i++;
		}
		table.appendChild(creerLigneTableau('td', ligne));
	}

	//Et on ferme le sandwich
	div.appendChild(table);
	formulaire.appendChild(div);

	//Maintenant que les listes sont dans le document, on peut les mettre à jour !
	updateListeEtat();
};

traiterFormulaire = function()
{
	//Là, on doit récupérer toutes les valeurs qui vont bien
	var formulaire = document.getElementById('formulaire');
	var id = 0;
	var obj = {};
	var infosExternes = {};

	//On commence par regarder du côté des inputs
	var inputs = formulaire.getElementsByTagName('input');
	for(var i = 0; i < inputs.length; i++)
	{
		var input = inputs[i];
		if(input.id == 'id')
			id = parseInt(input.value);
		else if(input.id != '€classe')
		{
			if(input.type == 'checkbox')
			{
				obj[input.id] = input.checked;
			} else
				obj[input.id] = input.value;
		}
	}

	if(id == 0 || Object.is(id, NaN))
	{
		alert('Pas encore implémenté');
		return false;
	}

	//On oublie pas les éventuels textarea
	var textareas = formulaire.getElementsByTagName('textarea');
	for(var i = 0; i < textareas.length; i++)
	{
		var textarea = textareas[i];
		obj[textarea.id] = textarea.value;
	}

	//Il faut faire aussi les sélects
	var selects = formulaire.getElementsByTagName('select');
	for(var i = 0; i < selects.length; i++)
	{
		var item = selects[i];
		if(item.value == '_null')
			obj[item.id] = null;
		else
			obj[item.id] = item.value;
	}

	//On prépare les données a envoyer au serveur
	//D'abord la quête :
	var objQuete = {id: id, classe: 'Quete', values: {nom: obj.nom}}; //Object { nom: "La quête de la ligne 3 bis", conditionId: 1 }
	//Puis on fait les états et les conditions
	var infosEtats = {};
	for(var key in obj)
	{
		if(key != 'nom')
		{
			var analyse = key.split('-');
			var idEtat = parseInt(analyse[1]);
			if(infosEtats[idEtat] == undefined)
				infosEtats[idEtat] = {};
			infosEtats[idEtat][analyse[0]] = obj[key];
		}
	}

	//Maintenant, on fait le tri :)
	var objEtats = [];
	var objConditions = [];
	var destinationSuivante = -1;
	var idConditionNext = getMaxId(contenuStocke.Condition) + 1;
	var etatsAVoir = [0];
	var etatDejaVu = [];
	var idCondition;
	while(etatsAVoir.length != 0)
	{
		idEtat = etatsAVoir.shift();
		if(etatDejaVu.indexOf(idEtat) == -1)
		{
			etatDejaVu.push(idEtat);
			var item = infosEtats[idEtat];
			if(!item['final'])
			{
				if(item['€idCondition'] == undefined)
				{
					idCondition = idConditionNext;
					idConditionNext++;
				} else
					idCondition = parseInt(item['€idCondition']);
				var objCondition = {id: idCondition, classe: 'Condition', values: {niveau: null, etatId: parseInt(item['Destination']), stationId: parseInt(item['Station'])}, valuesExt: {}};
				if(idEtat == 0) //C'est celui attaché à la quête
				{
					objQuete.values.conditionId = idCondition;
					objCondition.valuesExt.ConditionDEtat = [];
				} else {
					objCondition.valuesExt.ConditionDEtat = [parseInt(idEtat)];
					var objEtat = {id: parseInt(idEtat), classe: 'Etat', values: {queteId: id}, valuesExt: {ConditionDEtat: [idCondition]}};
					for(var attrOfEtat in listeClasses.Etat.attr)
					{
						objEtat.values[attrOfEtat] = item[attrOfEtat];
					}
					objEtats.push(objEtat);
				}
				objConditions.push(objCondition);
				etatsAVoir.push(parseInt(item['Destination']));
				destinationSuivante = parseInt(item['Destination']);
			}
		}
	}
	//Et on n'oublie pas l'état final
	var objEtat = {id: destinationSuivante, classe: 'Etat', values: {queteId: id}, valuesExt: {ConditionDEtat: []}};
	for(var attrOfEtat in listeClasses.Etat.attr)
	{
		objEtat.values[attrOfEtat] = item[attrOfEtat];
	}
	objEtats.push(objEtat);

	var envoi = [].concat(objEtats).concat(objConditions).concat(objQuete);
	socket.emit('putInfos', envoi);
}

socket.on('putInfos', function() {
	demanderClasse("Quete");
});

demanderClasse("Quete");
