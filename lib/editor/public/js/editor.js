/*
Copyright 2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var socket = io();
var listeClasses = {};
var contenuStocke = {};
var classeAAfficher = null;
afficherLien = true;

function demanderClasses()
{
	socket.emit('listeClasses');
}

function demanderClasse(classe)
{
	socket.emit('contenu', classe);
	classeAAfficher = classe;
}

socket.on('listeClasses', function(obj){
	listeClasses = obj;
	var ul = document.getElementById('liste_classes');
	if(!!ul)
	{
		ul.innerHTML = '';
		Object.keys(obj).forEach(function(item) {
			var li = document.createElement('li');
			li.innerHTML = '<a onclick="demanderClasse(\''+item+'\')">'+item+'</a>';
			ul.appendChild(li);
		});
	}
});

function afficherNomLisible(classe, id)
{
	if(!!contenuStocke[classe] && !!contenuStocke[classe][id] && !!contenuStocke[classe][id].nom)
		return contenuStocke[classe][id].nom;
	else
		return id;
}

function afficherClasse(classe)
{

	var informations = document.getElementById('informations');
	informations.innerHTML = '';
	//On vide aussi le formulaire
	var formulaire = document.getElementById('formulaire');
	formulaire.innerHTML = '';

	var entete = document.createElement('tr');

	//Colonnes des actions
	var th = document.createElement('th');
	entete.appendChild(th);

	//On a toujours un Id
	var th = document.createElement('th');
	th.innerHTML = 'id';
	entete.appendChild(th);

	var attr = listeClasses[classe].attr;
	for(var key in attr)
	{
		var th = document.createElement('th');
		th.innerHTML = key;
		entete.appendChild(th);
	}

	var assoInterne = listeClasses[classe].assoInterne;
	var assoExterne = listeClasses[classe].assoExterne;
	if(afficherLien)
	{	
		for(var key in assoInterne)
		{
			var th = document.createElement('th');
			th.innerHTML = key;
			entete.appendChild(th);
		}
		for(var key in assoExterne)
		{
			var th = document.createElement('th');
			th.innerHTML = key;
			entete.appendChild(th);
		}
	}

	informations.appendChild(entete);

	//On récupère aussi les informations de la classe
	var attr = listeClasses[classe].attr;
	var attrkey = Object.keys(attr);
	var aIntkey = Object.keys(assoInterne);
	var aExtkey = Object.keys(assoExterne);
	for(var id in contenuStocke[classe])
	{
		var ligne = document.createElement('tr');
		var td = document.createElement('td');
		td.innerHTML = '<a onclick="afficherFormulaire(\''+classe+'\', '+id+');">Modifier</a> - <a onclick="supprimerItem(\''+classe+'\', '+id+');">Supprimer</a>';
		ligne.appendChild(td);
		var td = document.createElement('td');
		td.innerHTML = id;
		ligne.appendChild(td);
		var item = contenuStocke[classe][id];
		for(var key in item)
		{			
			if(attrkey.indexOf(key) != -1)
			{
				var td = document.createElement('td');
				td.innerHTML = item[key];
				ligne.appendChild(td);
			}
			else if(aIntkey.indexOf(key) != -1)
			{
				if(afficherLien)
				{
					var td = document.createElement('td');
					var classeExterne = assoInterne[key];
					td.innerHTML = afficherNomLisible(classeExterne, item[key]);
					ligne.appendChild(td);
				}
			} else if(aExtkey.indexOf(key) != -1)
			{
				if(afficherLien)
				{
					var td = document.createElement('td');
					var classeExterne = assoExterne[key];
					td.innerHTML = item[key].map(function(idLocal) { return afficherNomLisible(classeExterne, idLocal); }).join(', ');
					ligne.appendChild(td);
				}
			} else
			{
				var td = document.createElement('td');
				td.innerHTML = item[key];
				ligne.appendChild(td);
			}
		}
		informations.appendChild(ligne);
	}

	var ligne = document.createElement('tr');
	var td = document.createElement('td');
	td.innerHTML = '<a onclick="afficherFormulaire(\''+classe+'\', 0);">Ajouter un élément</a>';
	td.setAttribute('colspan', 1 + 1 + attrkey.length + aIntkey.length + aExtkey.length);
	ligne.appendChild(td);
	informations.appendChild(ligne);
}

affichageFormulaireCustom = function(classe, id) {};

socket.on('contenu', function(info) {
	var classe = info.classe;
	var assoInterne = listeClasses[classe].assoInterne;
	var assoExterne = listeClasses[classe].assoExterne;
	var infosNecessaires = [];
	for(var key in assoInterne)
	{
		var c = assoInterne[key];
		if(!contenuStocke[c] && infosNecessaires.indexOf(c) == -1)
		{
			socket.emit('contenu', c); //TODO : Retarder l'affichage tant qu'on n'a pas reçu ces infos
			infosNecessaires.push(c);
		}
	}
	for(var key in assoExterne)
	{
		var c = assoExterne[key];
		if(!contenuStocke[c] && infosNecessaires.indexOf(c) == -1)
		{
			socket.emit('contenu', c); //TODO : Retarder l'affichage tant qu'on n'a pas reçu ces infos
			infosNecessaires.push(c);
		}
	}

	if(!contenuStocke[classe])
		contenuStocke[classe] = {};
	info.items.forEach(function(item) {
		var obj = {};
		var id = 0;
		for(var key in item)
		{
			if(key == 'id')
				id = parseInt(item[key]);
			else
				obj[key] = item[key];
		}
		if(id != 0)
			contenuStocke[classe][id] = obj;
	});

	if(classeAAfficher == classe)
	{
		afficherClasse(classe);
	}
});

function supprimerItem(classe, id)
{
	socket.emit('putInfo', {classe: classe, id: id, values: 'delete'});
}

function afficherFormulaire(classe, id)
{
	//Id = 0, alors nouvel ajout
	var formulaire = document.getElementById('formulaire');
	formulaire.innerHTML = '';
	//On commence par ajouter l'id
	ajouterChamp('id', 'index', id);

	//On récupère la valeur de l'item si nécessaire
	var values = null;
	if(!!id && !!contenuStocke[classe] && !!contenuStocke[classe][id])
	{
		values = contenuStocke[classe][id];
	}

	//Puis, tous les attributs de la table
	var attr = listeClasses[classe].attr;
	for(var key in attr)
	{
		var type = attr[key];
		ajouterChamp(key, type, (!!values ? values[key] : ''));
	}

	//Puis, toutes les associations internes
	var assoInterne = listeClasses[classe].assoInterne;
	var assoExterne = listeClasses[classe].assoExterne;
	if(afficherLien)
	{
		for(var key in assoInterne)
		{
			var classeExt = assoInterne[key];
			ajouterChamp(key, 'lien', (!!values ? values[key] : ''), classeExt);
		}

		//Enfin, toutes les associations externes
		for(var key in assoExterne)
		{
			var classeExt = assoExterne[key];
			ajouterChamp(key, 'multilien', (!!values ? values[key] : ''), classeExt);
		}
	}

	//Affichage custom
	affichageFormulaireCustom(classe, id)

	//Les champs cachés
	ajouterChamp('€classe', 'cache', classe);
	//Enfin, le bouton submit
	var button = document.createElement('button');
	button.addEventListener('click', traiterFormulaire);
	button.innerHTML = "Enregistrer";
	formulaire.appendChild(button);
}

function ajouterChamp(id, type, value, attribut, labelVisible)
{
	var formulaire = document.getElementById('formulaire');
	formulaire.appendChild(faireChamp(id, type, value, attribut, labelVisible));
}

function ajouterOptionsDansSelect(item, liste, selectedValue, addNull)
{
	if(addNull == undefined)
		addNull = false;

	if(addNull) //Ajout de la valeur null
	{
		var opt = document.createElement('option');
		opt.value = '_null';
		opt.innerHTML = '&nbsp;';
		item.appendChild(opt);
	}

	for(var key in liste) //On va mettre comme option chaque value du tableau
	{
		key = parseInt(key);
		var opt = document.createElement('option');
		opt.value = key;
		opt.innerHTML = (!!liste[key].nom ? liste[key].nom : key);
		if(selectedValue.indexOf(key) != -1)
			opt.setAttribute('selected', true);
		item.appendChild(opt);
	}
}

function faireChamp(id, type, value, attribut, labelVisible, listeChoix)
{
	if(labelVisible == undefined)
		labelVisible = true;

	var div = document.createElement('div');

	//Label
	if(type != 'cache' && labelVisible)
	{
		var label = document.createElement('label');
		label.setAttribute('for', id);
		label.innerHTML = id;
		div.appendChild(label);
	}

	//Input
	var item;
	if(type == 'lien' || type == 'multilien') //attribut = Classe Externe
	{
		var liste = (!!listeChoix ? listeChoix : contenuStocke[attribut])
		if(!!liste)
		{
			item = document.createElement('select');
			item.className = 'select-'+attribut;
			var addNull = false;
			if(type == 'multilien')
				item.setAttribute('multiple', true);
			else //C'est un lien
			{
				value = [value];
				addNull = true;
			}
			ajouterOptionsDansSelect(item, liste, value, addNull); //On va mettre comme option chaque value du tableau
		} else { //On va le traiter comme un nombre
			type = 'number';
			item = document.createElement('input');
		}
	} else {
		if(type == 'textarea')
		{
			item = document.createElement('textarea');
			if(!!value)
				item.innerHTML = value;
		}
		else
			item = document.createElement('input');
	}

	item.name = id;
	item.id = id;
	if(type != 'lien' && type != 'textarea')
	{
		if(type == 'index')
		{
			item.setAttribute('readonly', true);
		} else if(type == 'cache')
		{
			item.type = 'hidden';
		} else if(type == 'boolean')
		{
			item.type = 'checkbox';
			item.value = 'true';
			if(!!value && value == true)
				item.setAttribute('checked', true);
		} else 
			item.type = type;
		if(!!value && type != 'boolean')
			item.value = value;
	}

	div.appendChild(item);

	return div;
}

traiterFormulaire = function()
{
	//Là, on doit récupérer toutes les valeurs qui vont bien
	var formulaire = document.getElementById('formulaire');
	var id = 0;
	var classe = '';
	var obj = {};
	var infosExternes = {};

	//On commence par regarder du côté des inputs
	var inputs = formulaire.getElementsByTagName('input');
	for(var i = 0; i < inputs.length; i++)
	{
		var input = inputs[i];
		if(input.id == 'id')
			id = input.value;
		else if(input.id == '€classe')
			classe = input.value;
		else
		{
			if(input.type == 'checkbox')
			{
				obj[input.id] = input.checked;
			} else
				obj[input.id] = input.value;
		}
	}

	//On oublie pas les éventuels textarea
	var textareas = formulaire.getElementsByTagName('textarea');
	for(var i = 0; i < textareas.length; i++)
	{
		var textarea = textareas[i];
		obj[textarea.id] = textarea.value;
	}

	//Il faut faire aussi les sélects
	var assoExterne = listeClasses[classe].assoExterne;
	var aExtkey = Object.keys(assoExterne);
	var selects = formulaire.getElementsByTagName('select');
	for(var i = 0; i < selects.length; i++)
	{
		var item = selects[i];
		if(aExtkey.indexOf(item.id) != -1)
		{
			var reponses = [];
			for(var j = 0; j < item.selectedOptions.length; j++)
				reponses.push(item.selectedOptions[j].value);
			infosExternes[item.id] = reponses;
		} else {
			if(item.value == '_null')
				obj[item.id] = null;
			else
				obj[item.id] = item.value;
		}
	}

	//Et maintenant, on envoie tout ça au serveur
	var envoi = {classe: classe, id: id, values: obj};
	if(infosExternes != {})
		envoi.valuesExt = infosExternes;
	socket.emit('putInfo', envoi);
}

socket.on('updateNeeded', function(classe) {
	demanderClasse(classe);
});
