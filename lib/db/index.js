/*
Copyright 2015-2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var config = require('../../config');
var Sequelize = require('sequelize');
var sequelize = new Sequelize(config.db);
var Promise = require('promise');

//Création du schéma de Sequelize

var Joueur = require('./classes/Joueur')(sequelize);
var Station = require('./classes/Station')(sequelize);
var Ligne = require('./classes/Ligne')(sequelize);
var Trajet = require('./classes/Trajet')(sequelize);
var Quete = require('./classes/Quete')(sequelize);
var Etat = require('./classes/Etat')(sequelize);
var Condition = require('./classes/Condition')(sequelize);
var QuetesDuJoueur = require('./classes/QuetesDuJoueur')(sequelize);
var Dialogue = require('./classes/Dialogue')(sequelize);

//On exporte
exports.Joueur = Joueur;
exports.Station = Station;
exports.Ligne = Ligne;
exports.Trajet = Trajet;
exports.Quete = Quete;
exports.Etat = Etat;
exports.Condition = Condition;
exports.QuetesDuJoueur = QuetesDuJoueur;
exports.Dialogue = Dialogue;

//Relations

Joueur.belongsTo(Station, {as: 'stationCourante'});
Joueur.belongsTo(Station, {as: 'stationPrecedente'});

Joueur.belongsToMany(Quete, {as: 'QuetesDuJoueur', through: QuetesDuJoueur});
Quete.belongsToMany(Joueur, {through: QuetesDuJoueur});
Joueur.belongsToMany(Etat, {as: 'EtatDuJoueur', through: 'EtatduJoueur'});
Etat.belongsToMany(Joueur, {through: 'EtatduJoueur'});

Station.belongsToMany(Ligne, {through: 'StationSurLigne'});
Ligne.belongsToMany(Station, {through: 'StationSurLigne'});

Trajet.belongsTo(Station, {as: 'depart'});
Trajet.belongsTo(Station, {as: 'arrivee'});
Trajet.belongsTo(Ligne); 

Station.hasMany(Trajet, {foreignKey: 'departId'});

Quete.belongsTo(Condition);
Condition.hasOne(Quete);

Etat.belongsToMany(Condition, {through: 'ConditionDEtat'});
Condition.belongsToMany(Etat, {through: 'ConditionDEtat'});

Quete.hasOne(Etat, {constraints: false});//L'état doit avoir son id de quête a un moment…

Condition.belongsTo(Etat);
Etat.hasOne(Condition);

Condition.belongsTo(Station);

Dialogue.belongsTo(Station);
Station.hasMany(Dialogue);
Dialogue.belongsTo(Etat);
Etat.hasOne(Dialogue);

//On va créer les premières données à la main
var m3bis, m3, m11, t3b;
var gambetta, pelleport, st_fargeau, pte_lilas;
var quete, etat1, etat2, etat3, etat4, condition0, condition1, condition2, condition3;
var dialogueQueteDebut, dialogueQueteFin;

exports.init = function() {
		return sequelize.sync().then(function(){
				Ligne.findOrCreate({ //Créations des lignes
						where: {nom: "M3Bis"},
						defaults: {}
				}).spread(function(ligne, nouveau) {
						m3bis = ligne;
				}).then(function() {
						return Ligne.findOrCreate({
								where: {nom: "M3"},
								defaults: {}
						});
				}).spread(function(ligne, nouveau) {
						m3 = ligne;
				}).then(function() {
						return Ligne.findOrCreate({
								where: {nom: "M11"},
								defaults: {}
						});
				}).spread(function(ligne, nouveau) {
						m11 = ligne;
				}).then(function() { //Création des stations
						return Station.findOrCreate({
								where: {nom: "Gambetta"},
								defaults: {}
						});
				}).spread(function(station, nouveau) {
						gambetta = station;
						if(nouveau)
								gambetta.setLignes([m3bis, m3]);
				}).then(function() {
						return Station.findOrCreate({
								where: {nom: "Pelleport"},
								defaults: {}
						});
				}).spread(function(station, nouveau) {
						pelleport = station;
						if(nouveau)
								pelleport.setLignes([m3bis]);
				}).then(function() {
						return Station.findOrCreate({
								where: {nom: "Saint-Fargeau"},
								defaults: {}
						});
				}).spread(function(station, nouveau) {
						st_fargeau = station;
						if(nouveau)
								st_fargeau.setLignes([m3bis]);
				}).then(function() {
						return Station.findOrCreate({
								where: {nom: "Porte des Lilas"},
								defaults: {}
						});
				}).spread(function(station, nouveau) {
						pte_lilas = station;
						if(nouveau)
								pte_lilas.setLignes([m3bis, m11]);
				}).then(function() {
						return sequelize.sync();
				}).then(function() { //Création des trajets
						return Trajet.findOrCreate({
								where: {departId: gambetta.id, arriveeId: pelleport.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 60);
								trajet.save();
						}
				}).then(function() {
						return Trajet.findOrCreate({
								where: {departId: pelleport.id, arriveeId: st_fargeau.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 120);
								trajet.save();
						}
				}).then(function() {
						return Trajet.findOrCreate({
								where: {departId: st_fargeau.id, arriveeId: pte_lilas.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 60);
								trajet.save();
						}
				}).then(function() {
						return Trajet.findOrCreate({
								where: {departId: pte_lilas.id, arriveeId: st_fargeau.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 120);
								trajet.save();
						}
				}).then(function() {
						return Trajet.findOrCreate({
								where: {departId: st_fargeau.id, arriveeId: pelleport.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 60);
								trajet.save();
						}
				}).then(function() {
						return Trajet.findOrCreate({
								where: {departId: pelleport.id, arriveeId: gambetta.id},
								defaults: {}
						});
				}).spread(function(trajet, nouveau) {
						if(nouveau)
						{
								trajet.setLigne(m3bis);
								trajet.set('duree', 60);
								trajet.save();
						}
				}).then(function() { //Création des dialogues
						return Dialogue.findOrCreate({
								where: {
										nomPersonnage: "Passant",
										texte: "Belle journée, n'est-ce pas ?¶Si vous ne savez pas quoi faire, pourquoi donc ne pas aller à Porte des Lilas ?"
								},
								defaults: {}
						});
				}).spread(function(dialogue, nouveau) {
						if(nouveau)
						{
								dialogue.setStation(pelleport);
								dialogue.save();
						}
				}).then(function() {
						return Dialogue.findOrCreate({
								where: {
										nomPersonnage: "Narrateur",
										texte: "Ça te dit un petit défi ?¶Il suffit de parcourir la ligne la plus courte du réseau : la ligne 3 bis !¶Rendez-vous à Gambetta en passant par toutes les stations ;)"
								},
								defaults: {}
						});
				}).spread(function(dialogue, nouveau) {
						dialogueQueteDebut = dialogue
				}).then(function() {
						return Dialogue.findOrCreate({
								where: {
										nomPersonnage: "Narrateur",
										texte: "Et voilà, ce n'était pas si dur ^^"
								},
								defaults: {}
						});
				}).spread(function(dialogue, nouveau) {
						dialogueQueteFin = dialogue
				}).then(function() { //Création d'une quête
						return Quete.findOrCreate({
								where: {nom: "La quête de la ligne 3 bis"},
								defaults: {}
						});
				}).spread(function(q, nouveau) {
						if(nouveau)
						{
								quete = q;
								Promise.all([
										Condition.create().then(function (c) {
												condition0 = c;
										}),
										Condition.create().then(function (c) {
												condition1 = c;
										}),
										Condition.create().then(function (c) {
												condition2 = c;
										}),
										Condition.create().then(function (c) {
												condition3 = c;
										}),
										Etat.create().then(function (e) {
												e.set('queteId', quete.id);
												e.set('resume', 'Aller à la station Saint-Fargeau');
												e.save();
												dialogueQueteDebut.setEtat(e);
												etat1 = e;
										}),
										Etat.create().then(function (e) {
												e.set('queteId', quete.id);
												e.set('resume', 'Aller à la station Pelleport');
												e.save();
												etat2 = e;
										}),
										Etat.create().then(function (e) {
												e.set('queteId', quete.id);
												e.set('resume', 'Aller à la station Gambetta');
												e.save();
												etat3 = e;
										}),
										Etat.create().then(function (e) {
												e.set('queteId', quete.id);
												e.set('final', true);
												e.save();
												dialogueQueteFin.setEtat(e);
												etat4 = e;
										})
								]).then(function() {
										return sequelize.sync();
								}).then(function() {
										condition0.setEtat(etat1);
										condition1.setEtat(etat2);
										condition2.setEtat(etat3);
										condition3.setEtat(etat4);

										condition0.setStation(pte_lilas);
										condition1.setStation(st_fargeau);
										condition2.setStation(pelleport);
										condition3.setStation(gambetta);

										etat1.setConditions([condition1]);
										etat2.setConditions([condition2]);
										etat3.setConditions([condition3]);

										quete.setCondition(condition0);
								});
						}
				});
		});
};
