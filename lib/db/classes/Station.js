/*
Copyright 2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var Sequelize = require('sequelize');
var Promise = require('promise');
var Station = function(sequelize)
{
	return sequelize.define('station', {
		nom: Sequelize.STRING
	}, {
		instanceMethods:
		{
			getActionsPossible: function()
			{
				var resultats = [];
				var _this = this;

				return this.getDialogues()
					.then(function(liste_dialogue) {
					liste_dialogue.forEach(
						function(dialogue)
						{
							resultats.push(
								{
									type: 'dialogue',
									nomPersonnage: dialogue.get('nomPersonnage'),
									texte: dialogue.get('texte'),
									idElement: dialogue.get('id')
								}
							);
						}
					)
				}).then(function() {
					return _this.getTrajets()
						.then(
						function(liste_trajet)
						{
							liste_trajet.forEach(
								function(trajet)
								{
									resultats.push(
										{
											type: 'deplacement',
											station: trajet.get('arriveeId'),
											ligne: trajet.get('ligneId'),
											duree: trajet.get('duree'),
											depart: trajet.get('departId'),
											correspondance: 0,
											idElement: trajet.get('id')
										}
									);
								}
							);
						}).then(
						function() //On va récupérer les infos de chaque station
						{
							var i = 0;
							return Promise.all(
								resultats.map(
									function(trajet, i)
									{
										if(trajet.type == 'deplacement')
										{
											return sequelize.models.station.findById(trajet.station) //On ne peut pas appeler directement Station, mais on peut utiliser ça ^^
												.then(
												function(s)
												{
													resultats[i].nomStation = s.get('nom');
												}
											);
										}
									}
								)
							);
						}).then(
						function() //Faut aussi récupérer la ligne
						{
							return Promise.all(
								resultats.map(
									function(trajet, i)
									{
										if(trajet.type == 'deplacement')
										{
											return Ligne.findById(trajet.ligne)
												.then(
												function(ligne)
												{
													resultats[i].nomLigne = ligne.get('nom');
												}
											);
										}
									}
								)
							).then(
								function()
								{
									return resultats;
								}
							);
						}
					)
				});
			}
		}
	});
};

module.exports = Station;
