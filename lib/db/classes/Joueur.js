/*
Copyright 2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var Sequelize = require('sequelize');
var Promise = require('promise');
var Joueur = function(sequelize)
{
	return sequelize.define
	('joueur', {
		token: Sequelize.STRING,
		pseudo: Sequelize.STRING,
		action: Sequelize.STRING,
		niveau: { type: Sequelize.INTEGER, defaultValue: 0 },
		xp: { type: Sequelize.INTEGER, defaultValue: 0 },
		dateProchaineAction: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
	}, {
		instanceMethods:
		{
			verifierQuetes: function(socket)
			{
				var _this = this;
				var quetesDejaVues = [];
				//On regarde d'abord pour les nouvelles quêtes
				this.getQuetesDuJoueur()
					.then(
					function(relationids)
					{
						return relationids.map(
							function(item)
							{
								quetesDejaVues.push(item.id);
							}
						);
					}).then(
					function() //On regarde d'abord si on peut prendre une nouvelle quête
					{
						var conditionId = {}; //Hack pour pas que le findAll ne renvoi rien ^^'
						if(quetesDejaVues.length != 0)
							conditionId = {id:{
								$notIn: quetesDejaVues
							}};
						return Quete.findAll({
							where: conditionId
						}).then(
							function(quetes)
							{
								return quetes.map(
									function(quete)
									{
										return quete.getCondition().then(
											function(condition)
											{
												if(condition.estVerifie(_this))
												{
													socket.emit('information', {domaine: 'Quête', message: 'Quête '+quete.id+' commencée.'});
													return _this.addQuetesDuJoueur(quete).then(
														function()
														{
															return condition.getEtat().then(
																function(etat)
																{
																	_this.addEtatDuJoueur(etat);
																	etat.entrerDansEtat(socket);
																}
															);
														}
													);
												}
											}
										);
									}
								);
							}
						);
					}
				).then( //Maintenant, on va regarder les états en cours
					function()
					{
						return _this.getEtatDuJoueur().then(
							function(etatsDuJoueur)
							{
								if(etatsDuJoueur.length > 0)
								{
									var etatsId = etatsDuJoueur.map(
										function(edj)
										{
											return edj.EtatduJoueur.etatId;
										}
									);
									return Etat.findAll({
										where: {
											id: {
												$in: etatsId
											}
										}
									}).then(
										function(etats)
										{
											return etats.forEach(
												function(etat)
												{
													return etat.getConditions().then(
														function(conditions)
														{
															var stop = false;
															for(var i = 0; !stop && i < conditions.length; i++)
															{
																var condition = conditions[i];
																if(condition.estVerifie(_this))
																{
																	stop = true;
																	_this.removeEtatDuJoueur(etat);
																	return condition.getEtat().then(
																		function(e)
																		{
																			e.entrerDansEtat(socket);
																			if(e.final) //Quête terminée
																			{
																				console.log('Quête '+e.queteId+' terminée !');
																				if(!!socket)
																					socket.emit('information', {domaine: 'Quête', message: 'Quête '+e.queteId+' terminée !'});
																				_this.ajouterXp(e.gainXp, socket);
																				console.log('Mise à jour de la Quête du joueur : joueurId : '+_this.id+' et queteId : '+e.queteId);
																				return QuetesDuJoueur.findOne({
																					where: {
																						joueurId: _this.id,
																						queteId: e.queteId
																					}
																				}).then(
																					function(qdj)
																					{
																						qdj.set('termine', true);
																						qdj.save();
																					}
																				);
																			} else {
																				_this.addEtatDuJoueur(e);
																				console.log('Quête '+e.queteId+' avance : Passage à l\'état '+e.id+' !');
																				if(!!socket)
																				{
																					socket.emit('information', {domaine: 'Quête', message: 'Quête '+e.queteId+' avance : Passage à l\'état '+e.id+' !'});
																				}
																			}
																		}
																	)
																}
															}
														}
													);
												}
											);
										}
									)
								}
							}
						)
					}
				);
			},
			getQuetes: function()
			{
				return this.getQuetesDuJoueur()
					.then(
					function(relationids)
					{
						return relationids.map(
							function(quete)
							{
								return {id: quete.id, nom: quete.nom, termine: quete.QuetesDuJoueur.termine};
							}
						);
					}
				);
			},
			getEtatQuete: function(idQuete, socket)
			{
				return this.getEtatDuJoueur()
					.then(
					function(listeEtats)
					{
						var resume = null;
						listeEtats.every(function(etat) {
							if(etat.queteId == idQuete)
							{
								resume = etat.getResume();
								return false;
							}
						});
						if(!!socket && resume != null)
							socket.emit('infoQuete', {id: idQuete, resume: resume});
					}
				);
			},
			ajouterXp: function(nb, socket)
			{
				var xp = this.xp + nb;
				var next = sequelize.models.joueur.xpNecessairePourNiveauSuivant(this.niveau);
				if(next != null && xp >= next) //On change de niveau :)
				{
					xp -= next;
					this.niveau++;
				}
				this.xp = xp;
				this.save();
				if(!!socket)
				{
					socket.emit('information', {domaine: 'Niveau', message: 'Niveau : '+this.niveau+' - XP : '+xp+'/'+next});
					socket.emit('experience', {niveau: this.niveau, actuel: xp, total: next});
				}
			}
		},
		classMethods: {
			xpNecessairePourNiveauSuivant: function(n){
				var niveaux = [10, 25, 45, 70, 100];
				if(n >= niveaux.length)
					return null;
				else
					return niveaux[n];
			}
		}
	}
	);
}

module.exports = Joueur;
