-- Ajout de niveau et xp dans table utilisateur
ALTER TABLE `joueurs` ADD `niveau` INT(11) NOT NULL DEFAULT '0' AFTER `action`, ADD `xp` INT(11) NOT NULL DEFAULT '0' AFTER `niveau`;

-- Ajout du gain d XP dans les états de quêtes
ALTER TABLE `etats` ADD `gainXp` INT(11) NULL DEFAULT NULL AFTER `introduction`;
UPDATE `etats` SET `gainXp` = '5' WHERE `etats`.`id` = 4;

-- Ajout du niveau dans les conditions
ALTER TABLE `conditions` ADD `niveau` INT(11) NOT NULL AFTER `id`;

-- Ajout du pseudo du joueur
ALTER TABLE `joueurs` ADD `pseudo` VARCHAR(255) NOT NULL AFTER `token`;

-- Renommage de introduction en resume dans etat
ALTER TABLE `etats` CHANGE `introduction` `resume` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
