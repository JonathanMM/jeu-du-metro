ALTER TABLE `etats` ADD `introduction` TEXT NULL AFTER `final`;

UPDATE `etats` SET `introduction` = 'Ça te dit un petit défi ? Il suffit de parcourir la ligne la plus courte du réseau : la ligne 3 bis ! Rendez-vous à Gambetta en passant par toutes les stations ;)' WHERE `etats`.`id` = 1 ;
UPDATE `etats` SET `introduction` = 'Et voilà, ce n''était pas si dur ^^' WHERE `etats`.`id` = 4 ;