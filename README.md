Le jeu du métro est une sorte de jeu de l'oie qui se passe dans le métro parisien.
On se déplace de station en station, en temps réel, on réalise des quêtes, on collectionne, etc…

Jeu sur navigateur, serveur en node.js. Sous licence AGPL 3+.

# Installation

Par défaut, le jeu nécessite d'avoir une base de données mysql en localhost nommé nouveaumetro, avec un utilisateur root sans mot de passe dessus. Si ce n'est pas votre cas, modifiez le début du fichier server.js. La BDD va créer la structure au premier lancement du jeu.

Pour l'installation des dépendances nécessaire, faites npm install avant le premier lancement.

# Mise à jour

Pour mettre à jour une instance déjà installée, accédez au dossier update, et executez les fichiers sql correspondant à la mise à jour que vous souhaitez réaliser.

# Lancement

Le jeu se lance à l'adresse http://localhost:3000

L'éditeur de niveau se trouve à l'adresse http://localhost:3001