/*
Copyright 2015-2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

function timer(fin) {
	var now = new Date();
	var diff = fin - now;
	if(diff <= 0)
	{
		document.getElementById('enCours-timer').innerHTML = '&nbsp;';
		document.getElementById('enCours-timer').style.backgroundColor = 'black';
		document.getElementById('ronfleur').play();
	}
	else
	{
		document.getElementById('enCours-timer').innerHTML = Math.floor(diff / 1000);
		document.getElementById('enCours-timer').style.backgroundColor = 'purple';
		var ms = diff % 1000 + 30;
		setTimeout(function(fin) { timer(fin) }, ms, fin);
	}
}

var socket = io();
function token()
{
	if(localStorage.tokenAuth)
		return localStorage.tokenAuth;
	else
	{
		var token = '';
		var char = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < 32; i++ )
			token += char.charAt(Math.floor(Math.random() * char.length));
		localStorage.tokenAuth = token;
		return token;
	}
}

socket.emit('identification', token());

socket.on('identification', function(infos) {
	if(infos.identifie) //Connexion réussie
		actualiserInterface();
	else
		alert("Erreur lors de l'identification : "+infos.erreur);
});

function demandeToken()
{
	socket.emit('demandeToken');
}

socket.on('infosJoueur', function(infos) {
	alert('Token = '+infos);
});

function demanderActions()
{
	socket.emit('demandeActions');
}

socket.on('actionsPossible', function(liste) {
	var ul = document.getElementById('actions');
	ul.innerHTML = '';
	if(liste.length == 0)
		ul.style.display = 'none';
	else
	{
		ul.style.display = 'block';
		liste.forEach(
			function(action)
			{
				var li = document.createElement('li');
				var msg = '';
				if(action.type == 'deplacement')
				{    
					var ligne;
					msg = 'Aller à '+action.nomStation;
					ligne = action.ligne;
					if(action.nomLigne.charAt(0) == 'M')
						li.style.backgroundImage = 'url("img/M_'+action.nomLigne.substr(1).toLowerCase()+'.png")';
					else
						msg += ' en métro via la ligne '+action.nomLigne;
					li.addEventListener('click', function() { faireAction('deplacement', action.idElement)});
				} else if(action.type == 'dialogue')
				{
					msg = 'Discuter avec '+action.perso;
					li.addEventListener('click', function() { faireAction('dialogue', action.idElement)});
				}
				li.innerHTML = msg;
				ul.appendChild(li);
			}
		);
	}
});

function faireAction(type, arg1)
{
	var obj = {type: type, idElement: arg1};
	socket.emit('faireAction', obj);
	effacerDialogue();
}

function actualiserInterface()
{
	demandeEnCours();
	demanderActions();
	demanderQuetes();
	demanderInfoPerso();
}

function demanderInfoPerso()
{
	socket.emit('experience');
}

socket.on('experience', function(obj) {
	var divNiveau = document.getElementById('joueur-niveau');
	divNiveau.innerHTML = obj.niveau;
	var divXp = document.getElementById('joueur-xp');
	var valeur = obj.actuel;
	if(obj.total != null)
		valeur += '/' + obj.total;
	divXp.innerHTML = valeur;
});

socket.on('actionEffectue', function(objet){
	if(objet.type == 'deplacement')
	{
		actualiserInterface();
	}
});

function demandeEnCours()
{
	socket.emit('actionEnCours');
}

var toFinDeplacement = null;

socket.on('actionEnCours', function(obj) {
	var icon = '';
	var msg = '';
	if(obj.type == 'deplacement')
	{
		//msg += ' vers '+obj.station;//+', arrivée prévue à : '+obj.fin;
		msg = obj.station;
		timer(new Date(obj.fin));
		if(toFinDeplacement != null)
			clearTimeout(toFinDeplacement);
		toFinDeplacement = setTimeout(actualiserInterface, ((new Date(obj.fin)).getTime() - (new Date()).getTime()));
		icon = '<img src="img/deplacement.png" alt="Depl." />';
		document.getElementById('enCours-action').style.backgroundColor = 'black';
	}
	else if(obj.type == 'attente')
	{
		//msg += ' à la station '+obj.station;
		msg = obj.station;
		icones = {metro: []};
		obj.lignes.forEach(function(nom) {
			if(nom.charAt(0) == 'M')
			{
				indice = nom.substr(1);
			} else
				indice = nom;

			icones.metro.push('<img src="img/M_'+indice.toLowerCase()+'.png" style="width: 1.5em" alt="'+nom+'" />');
		});
		if(icones.metro.length > 0)
			icon += '<img src="img/L_M.png" style="width: 1.5em" alt="Métro" /> '+icones.metro.join(' ');
		document.getElementById('enCours-action').style.backgroundColor = 'white';
	}
	document.getElementById('enCours-description').innerHTML = msg;
	document.getElementById('enCours-action').innerHTML = icon;
});

var tamponDialogue = {perso: '', repliques: []};

socket.on('dialogue', function(obj) {
	tamponDialogue.perso = obj.perso;
	var texte = obj.texte.split('¶');
	if(texte.length == 0)
		texte = [obj.texte];
	tamponDialogue.repliques = texte;
	voirDialogue();
});

function voirDialogue()
{
	if(tamponDialogue.repliques.length == 0)
		effacerDialogue(); //Plus rien à afficher
	else
	{
		var texte = tamponDialogue.repliques.shift();
		var div = document.getElementById('dialogue-texte');
		div.innerHTML = '<strong>'+tamponDialogue.perso+'</strong> : '+texte;
		var divSuite = document.getElementById('dialogue-action');
		if(tamponDialogue.repliques.length == 0) //Dernière réplique
			divSuite.innerHTML = 'Fin';
		else
			divSuite.innerHTML = 'Suite &gt;';
		document.getElementById('dialogue').style.display = 'block';
	}
};

function effacerDialogue()
{
	var div = document.getElementById('dialogue');
	div.style.display = 'none';
}

function demanderQuetes()
{
	socket.emit('quetesDuJoueur');
}

function voirResumeQuete(idQuete)
{
	var id = 'quete-info-'+idQuete;
	var li = document.getElementById(id);
	if(li == null) //N'existe pas, à créer
		socket.emit('demandeInfoQuete', idQuete);
	else //On supprime tout simplement
	{
		var ul = document.getElementById('quetes-liste');
		ul.removeChild(li);
	}
}

socket.on('infoQuete', function(item) {
	var id = 'quete-info-'+item.id;
	var ul = document.getElementById('quetes-liste');
	li = document.createElement('li');
	li.id = id;
	li.className = 'quete-resume';
	li.innerHTML = item.resume;
	var liQuete = document.getElementById('quete-'+item.id);
	var suivant = liQuete.nextSibling;
	if(suivant == null) //Dernier élément de la liste
		ul.appendChild(li);
	else
		ul.insertBefore(li, suivant);
});

socket.on('quetes', function(obj){
	var ul = document.getElementById('quetes-liste');
	ul.innerHTML = '';
	var termine = 0;
	obj.forEach(function(item) {
		var li = document.createElement('li');
		li.innerHTML = '#'+item.id+' : '+item.nom+' ('+(item.termine ? 'Terminé' : 'En cours')+')';
		li.id = 'quete-'+item.id;
		if(item.termine)
			li.className = 'quete-termine';
		else
			li.className = 'quete-en-cours';
		li.addEventListener('click', function() { voirResumeQuete(item.id); });
		ul.appendChild(li);
		if(item.termine)
			termine++;
	});
	document.getElementById('joueur-quetes').innerHTML = termine + '/' + obj.length;
});

function putZero(n)
{
	if(n <= 9)
		return '0'+n;
	else
		return n;
}

socket.on('information', function(info) {
	var ul = document.getElementById('logs');
	var auj = new Date();
	var li = document.createElement('li');
	li.innerHTML = '<em>'+putZero(auj.getHours())+':'+putZero(auj.getMinutes())+':'+putZero(auj.getSeconds())+'</em> <strong>['+info.domaine+']</strong> '+info.message;
	ul.appendChild(li);
	if(info.domaine == 'Quête')
		demanderQuetes();
});

window.addEventListener('load', function() {
	var pseudo = document.getElementById('joueur-pseudo');
	if(!!localStorage.pseudo)
		pseudo.innerHTML = localStorage.pseudo;
	else
		localStorage.pseudo = 'Anonyme';

	pseudo.addEventListener('click', function() {
		var pseudoJoueur = prompt("Changer le pseudo", localStorage.pseudo);
		if(pseudoJoueur.length > 0)
		{
			pseudo.innerHTML = pseudoJoueur;
			localStorage.pseudo = pseudoJoueur;
			socket.emit('pseudo', pseudoJoueur);
		}
	});
});
