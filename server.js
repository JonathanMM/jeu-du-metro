/*
Copyright 2015-2016 Les développeurs du jeu du métro

    This file is part of Jeu du métro.

    Jeu du métro is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jeu du métro is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with Jeu du métro.  If not, see <http://www.gnu.org/licenses/>.
*/

var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Promise = require('promise');
var db = require('./lib/db');
var config = require('./config');

//On configure express pour qu'il réponde bien les bonnes pages
app.use(express.static('public'));

Joueur = db.Joueur;
Station = db.Station;
Ligne = db.Ligne;
Trajet = db.Trajet;
Quete = db.Quete;
Etat = db.Etat;
QuetesDuJoueur = db.QuetesDuJoueur;

db.init().then(function() { //Tout est chargé, on peut lancer le serveur

	//Lancement du serveur http
	var port = config.port;
	var port_editeur = config.editorPort;

	//On lance l'éditeur
	require('./lib/editor')(db, port_editeur);

	http.listen(port, function(){
		console.log('[EXPRESS] Serveur démarré sur le port '+port);
	});

	//Lancement du serveur socket.io
	io.on('connection', function(socket){
		console.log('[SOCKET] Un utilisateur vient de se connecter');
		socket.on('identification', function(token)
				  {
			Joueur
				.findOrCreate({
				where: {token: token},
				defaults: {}
			})
				.spread(function(joueur, nouveau){
				if(nouveau)
				{
					Station.findOne({
						where: {nom: "Pelleport"}
					}).then(function(station) {
						if(station)
							joueur.setStationCourante(station);
					});
					console.log('[JOUEUR] #'+socket.id+' Nouveau joueur ! Token : '+token);
				}
				else
					console.log('[JOUEUR] #'+socket.id+' Connexion de '+token);
				socket.joueur = joueur;
				socket.emit('identification', {identifie: true});
			});
		});

		socket.on('demandeToken', function(){
			console.log('[JOUEUR] #'+socket.id+' Demande de token');
			socket.emit('infosJoueur', socket.joueur.get('token'));
		});

		socket.on('actionEnCours', function(){
			var datePA = socket.joueur.get('dateProchaineAction');
			var action = socket.joueur.get('action');
			console.log('fin action : '+datePA);
			console.log('actuellement : '+(new Date()));
			if(datePA < new Date()) //Rien
			{
				socket.joueur.getStationCourante().then(function(station) {
					return station.getLignes().then(function(lignes) {
						var nom_ligne = lignes.map(function(ligne) {
							return ligne.get('nom');
						});
						socket.emit('actionEnCours', {type: 'attente', station: station.get('nom'), lignes: nom_ligne});
					});
				});
				return null;
			}

			if(action == 'deplacement')
			{
				socket.joueur.getStationCourante()
					.then(function(station) {
					var datePA = socket.joueur.get('dateProchaineAction');
					var action = socket.joueur.get('action');
					var obj = {
						type: action,
						fin: datePA,
						station: station.get('nom')
					};
					socket.emit('actionEnCours', obj);
				});
			} else 
				socket.emit('actionEnCours', obj);
		});

		socket.on('experience', function() {
			socket.emit('experience', {niveau: socket.joueur.niveau, actuel: socket.joueur.xp, total: Joueur.xpNecessairePourNiveauSuivant(socket.joueur.niveau)});
		});

		socket.on('demandeActions', function(){
			var datePA = socket.joueur.get('dateProchaineAction');
			if(datePA > new Date()) //Une action est déjà en cours
			{
				socket.emit('actionsPossible', []);
				return null;
			}
			socket.joueur.getStationCourante().then(function(station) {
				//On va vérifier pour les quêtes
				socket.joueur.verifierQuetes(socket);

				station.getActionsPossible()
					.then(function(resultats) {
					console.log('[JOUEUR] #'+socket.id+' Liste des actions possible : ');
					resultats.forEach(function(r) {
						if(r.type == 'deplacement')
							console.log('Trajet vers '+r.nomStation+' en métro via la ligne '+r.nomLigne+' en '+(r.duree+r.correspondance)+'sec');
						else
							console.log('Action possible : ' + JSON.stringify(r));
					});
					socket.actionsPossible = resultats;
					socket.emit('actionsPossible', resultats.map(function(item) { 
						var obj = {type: item.type, idElement: item.idElement};
						if(item.type == 'deplacement')
						{
							obj.nomStation = item.nomStation;
							obj.nomLigne = item.nomLigne;
						} else if(item.type == 'dialogue')
						{
							obj.perso = item.nomPersonnage;
						}
						return obj;
					}));
				});
			});
		});

		function seDeplacer(item)
		{
			var stationId = socket.joueur.get('stationCouranteId');
			console.log('[JOUEUR] #'+socket.id+' Déplacement');
			console.log('Temps de déplacement : '+item.duree+'sec');
			socket.joueur.set('stationCouranteId', item.station);
			socket.joueur.set('stationPrecedenteId', stationId);
			var now = new Date();
			now.setSeconds(now.getSeconds() + item.duree);
			socket.joueur.updateAttributes({
				stationCouranteId: item.station,
				stationPrecedenteId: stationId,
				dateProchaineAction: now,
				action: 'deplacement'
			});
			socket.emit('actionEffectue', {type: 'deplacement'});
			socket.joueur.ajouterXp(1, socket);
			Station.findById(item.station).then(function(station) {
				socket.emit('information', {domaine: 'Déplacement', message: 'Déplacement vers '+station.get('nom')});
			});
		}

		function communiquer(item)
		{
			console.log('[JOUEUR] #'+socket.id+' Dialogue');
			socket.emit('dialogue', {perso: item.nomPersonnage, texte: item.texte});
			socket.emit('actionEffectue', {type: 'dialogue'});
			socket.emit('information', {domaine: 'Dialogue', message: 'Discussion avec '+item.nomPersonnage});
		}

		socket.on('faireAction', function(objet) {
			var actionsPossible = socket.actionsPossible;
			var i = 0;
			var stop = false;
			while(!stop && i < actionsPossible.length)
			{
				var item = actionsPossible[i];
				if (objet.type == item.type && objet.idElement == item.idElement)
				{
					stop = true;
					if(objet.type == 'deplacement')
						seDeplacer(item);
					else if(objet.type == 'dialogue')
						communiquer(item);
				}
				i++;
			}
		});

		socket.on('quetesDuJoueur', function() {
			console.log('[JOUEUR] #'+socket.id+' Demande de ses qutêtes');
			socket.joueur.getQuetes().then(
				function(infos)
				{
					socket.emit('quetes', infos);
				}
			);
		});

		socket.on('demandeInfoQuete', function(idQuete) {
			socket.joueur.getEtatQuete(idQuete, socket);
		});

		socket.on('pseudo', function(pseudo) {
			socket.joueur.pseudo = pseudo;
			socket.joueur.save();
		});
	});
});
